#!/usr/bin/python3.6
# Projeto 6
# Author: Aloisio Dourado Neto
# Discipline: Computer Vision/UnB
# Professor: Teo de Campos
# Mail: aloisio.dourado.bh@gmail.com
# Created Time: Tue 7 Jun 2018

import cv2
import sys
import pandas as pd
import numpy as np
import os
import math
from trackers import TemplateMatchTracker, MeanShiftTracker, MultiScaleTMTracker
import argparse

# Global Parameters

PROJECT_NAME = "Computer Vision Demonstration Project 6"
PROJECT_DESC = "Car Trackers"

BASE_DIR = "PD8-files"
CAR = "2"
TRACKER = "MS"
OFFSET = 20
USE_KALMAN = True

def intercept(r1p1, r1p2, r2p1, r2p2):
    x_overlap = max(0, min(r1p2[0], r2p2[0]) - max(r1p1[0], r2p1[0]))
    y_overlap = max(0, min(r1p2[1], r2p2[1]) - max(r1p1[1], r2p1[1]))
    return x_overlap * y_overlap


def union(r1p1, r1p2, r2p1, r2p2):
    area1 = (r1p2[0] - r1p1[0]) * (r1p2[1] - r1p1[1])
    area2 = (r2p2[0] - r2p1[0]) * (r2p2[1] - r2p1[1])
    return area1 + area2 - intercept(r1p1, r1p2, r2p1, r2p2)

def overlap(r1p1, r1p2, r2p1, r2p2):
    return intercept(r1p1, r1p2, r2p1, r2p2)/union(r1p1, r1p2, r2p1, r2p2)


def track():

    gt_df = pd.read_csv(os.path.join(BASE_DIR,"gtcar"+CAR+".txt"), header=None)

    gt_df['x']=gt_df[2] - gt_df[0]
    gt_df['y']=gt_df[3] - gt_df[1]

    files_dir = os.path.join(BASE_DIR,"car"+CAR)

    files = [os.path.join(files_dir, f) for f in os.listdir(files_dir) if os.path.isfile(os.path.join(files_dir, f)) and f[-3:]=='jpg']
    files.sort()

    # Read first frame
    img = cv2.imread(files[0])

    cv2.imshow("Tracking", img)

    # Define an initial bounding box
    bbox = (gt_df.iloc[0][0], gt_df.iloc[0][1], gt_df.iloc[0][2], gt_df.iloc[0][3])

    p1 = (int(bbox[0]), int(bbox[1]))
    p2 = (int(bbox[2]), int(bbox[3]))

    img2 = img.copy()

    cv2.rectangle(img2, p1, p2, (255, 0, 0), 1, 1)
    cv2.imshow("Tracking", img2)

    # Initialize tracker with first frame and bounding box
    if TRACKER == 'TM':
        tracker = TemplateMatchTracker(img, bbox, use_kalman=USE_KALMAN, search_offset=OFFSET)
    elif TRACKER == 'MSTM':
        tracker = MultiScaleTMTracker(img, bbox, use_kalman=USE_KALMAN, search_offset=OFFSET)
    elif TRACKER == 'MS':
        tracker = MeanShiftTracker(img, bbox, use_kalman=USE_KALMAN)

    sum_overlap = 0.0
    failure_count = 0

    i = 0
    valid_frames =0
    for file in files[0:]:

        if not np.isnan(gt_df.iloc[i][0]):
            gt1 = (int(gt_df.iloc[i][0]), int(gt_df.iloc[i][1]))
            gt2 = (int(gt_df.iloc[i][2]), int(gt_df.iloc[i][3]))
        else:
            gt1 = (0, 0)
            gt2 = (0, 0)

        i += 1

        # Read a new frame
        img = cv2.imread(file)

        # Start timer
        timer = cv2.getTickCount()

        # Update tracker
        bbox, kalman_bbox, search_box = tracker.update(img)


        # Calculate Frames per second (FPS)
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);

        p1 = (int(bbox[0]), int(bbox[1]))
        p2 = (int(bbox[2]), int(bbox[3]))

        k1 = (int(kalman_bbox[0]), int(kalman_bbox[1]))
        k2 = (int(kalman_bbox[2]), int(kalman_bbox[3]))

        s1 = (int(search_box[0]), int(search_box[1]))
        s2 = (int(search_box[2]), int(search_box[3]))


        img2 = img.copy()
        cv2.rectangle(img2, p1, p2, (255, 0, 0), 1, 1)
        cv2.rectangle(img2, k1, k2, (0, 255, 0), 1, 1)
        cv2.rectangle(img2, s1, s2, (0, 0, 255), 1, 1)
        cv2.rectangle(img2, gt1, gt2, (128, 128, 128), 1, 1)

        if not np.isnan(gt_df.iloc[i-1][0]):
            valid_frames += 1
            ovl = overlap(p1, p2, gt1, gt2)
            sum_overlap += ovl

            if ovl == 0:
                failure_count += 1

                bbox = (gt1[0], gt1[1], gt2[0], gt2[1])
                tracker.reset(img, bbox)

        rs= math.e ** (-30*failure_count/i)

        mean_overlap = sum_overlap/valid_frames

        cv2.putText(img2, type(tracker).__name__, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (50, 150, 80), 2);
        cv2.putText(img2, "Mean Overlap : %.4f" % mean_overlap, (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (50, 150, 80), 2);
        cv2.putText(img2, "Robustness : %.4f" % rs, (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (50, 150, 50), 2);
        cv2.putText(img2, "Fail Count : %d" % failure_count, (10, 65), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (50, 150, 80), 2);

        # Display result
        cv2.imshow("Tracking", img2)

        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27: break

    print("\nMean Overlap : %.4f" % mean_overlap)
    print("Robustness : %.4f" % rs)

    tracker_name = type(tracker).__name__
    if USE_KALMAN:
        tracker_name += '+Kalman'


    if os.path.isfile('results.csv'):
        df=pd.read_csv('results.csv')
        df= df.append(pd.DataFrame({'car':[CAR],'tracker':[tracker_name], 'offset':[OFFSET], 'mean_overlap':[mean_overlap], 'robustness':[rs]}))
    else:
        df= pd.DataFrame({'car':[CAR],'tracker':[tracker_name], 'offset':[OFFSET], 'mean_overlap':[mean_overlap], 'robustness':[rs]})
    df.to_csv('results.csv',index=False)

    cv2.putText(img2, "PRESS ANY KEY TO END!!!", (10, 210), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (50, 170, 50), 2);
    cv2.imshow("Tracking", img2)
    cv2.waitKey(0) & 0xff
    cv2.destroyAllWindows()


# Main Function
def Run():

    global BASE_DIR, CAR, TRACKER, OFFSET, OFFSET, USE_KALMAN

    print("\n%s\n%s" % (PROJECT_NAME, PROJECT_DESC))

    print("\nTested with python3.5.2, opencv-python 3.4.1")

    print("\nCurrent opencv-python version: %s\n" % cv2.__version__)

    parser = argparse.ArgumentParser()
    parser.add_argument("--base_dir", help="Base image directory.", type=str, default="PD8-files", required=False)
    parser.add_argument("--car", help="Car sequence to be used.", type=str, default="1", required=False,choices=['1','2'])
    parser.add_argument("--tracker", help="TM: Template Match Tracker (default), "
                                          "MSTM: Multi Scale Template Match Tracker",
                        type=str, default="TM", required=False, choices=['TM','MSTM'])
    parser.add_argument("--offset", help="Int offset for Template Match Tracker. Default 20. ", type=int, default=20, required=False)
    parser.add_argument("--use_kalman", help="If it is to use kalman filter after tracker [Y|N]. Default Y. ", type=str, default="Y", required=False)
    args = parser.parse_args()

    BASE_DIR = args.base_dir
    CAR = args.car
    TRACKER = args.tracker
    OFFSET = args.offset
    USE_KALMAN = args.use_kalman.upper() in ['YES', 'Y']

    print("\nParameters:")
    print("Base Dir:", BASE_DIR)
    print("Car:", CAR)
    print("Tracker:", TRACKER)
    print("Offset:", OFFSET)
    print("Use Kalman Filter?",USE_KALMAN)

    track()

if __name__ == '__main__':
  Run()