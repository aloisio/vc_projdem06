import pandas as pd
import numpy as np

import matplotlib.pylab as plt

df = pd.read_csv("results.csv")

#for car in df.car.unique():
for car in [2]:

    for tracker in df.tracker.unique():

        if tracker[-6:]=='Kalman':
            marker='o'
        else:
            marker = 'd'

        if tracker[:5]=='Templ':
            color='blue'
        else:
            color = 'red'

        print(tracker)
        print(color)
        df_plot = df.loc[(df.car==car) & (df.tracker==tracker)]

        p1= plt.plot(df_plot.robustness,df_plot.mean_overlap, color=color, label = tracker,marker=marker,linewidth=1)

        for index, row in df_plot.iterrows():
            p1= plt.text(row.robustness+.001, row.mean_overlap+.001, str(row.offset))
        #p1= plt.scatter(df_plot.robustness, df_plot.mean_overlap, s=df_plot.offset, label = tracker)

    #1
    #plt.ylim((0.55,1))
    #plt.xlim((0.55,1))
    #2
    plt.ylim((0.45,1))
    plt.xlim((0.45,1))
    plt.ylabel("Mean Overlap")
    plt.xlabel("Robustness")
    plt.legend()
    plt.show()


