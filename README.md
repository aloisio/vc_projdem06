# Projeto Demonstrativo 6 - Rastreamento de Imagens

# Instruções

## Conteúdo
0. [Requisitos](##Requisitos)
0. [Estrutura de Pastas](##Estrutura de Pastas)
0. [Instruções de Execução](##Execucao)

## Requisitos
1. Ubuntu 16.04 LTS ou Superior
2. Python 3.4 (numpy, pandas)
3. OpenCV 3


## Estrutura de Pastas
É esperado que as imagens estejam organizadas da seguinte forma:

```Shell
    |--PD8-Files               
       |--car1                 #Imagens para o carro 1
       |--car2                 #Imagens para o carro 2
       gtcar1.txt              #Ground truth do carro 1
       gtcar2.txt              #Ground truth do carro 2
    trackers.py                #Código fonte dos rastreadores
    tracker.py                 #Programa principal, executa os rastreadores    
```


## Instruções de Execução

Para ver a relação completa dos parâmetros digite:

```Shell
python tracker.py -h
```

1. Rastreamento do carro 1 com TemplateMatch, offset default, sem Filtro de Kalman:

    ```shell
    python tracker.py --car=1 --tracker=TM --use_kalman=N
    ```
    
2. Rastreamento do carro 1 com MultiScaleTemplateMatch, offset default, sem Filtro de Kalman:

    ```shell
    python tracker.py --car=1 --tracker=MSTM --use_kalman=N
    ```
    
3. Rastreamento do carro 1 com TemplateMatch, offset default, com Filtro de Kalman:

    ```shell
    python tracker.py --car=1 --tracker=TM
    ```
    
4. Rastreamento do carro 1 com MultiScaleTemplateMatch, offset default, com Filtro de Kalman:

    ```shell
    python tracker.py --car=1 --tracker=MSTM
    ```

5. Rastreamento do carro 2 com TemplateMatch, offset default, sem Filtro de Kalman:

    ```shell
    python tracker.py --car=2 --tracker=TM --use_kalman=N
    ```
    
6. Rastreamento do carro 2 com MultiScaleTemplateMatch, offset default, sem Filtro de Kalman:

    ```shell
    python tracker.py --car=2 --tracker=MSTM --use_kalman=N
    ```
    
7. Rastreamento do carro 2 com TemplateMatch, offset default, com Filtro de Kalman:

    ```shell
    python tracker.py --car=2 --tracker=TM
    ```
    
8. Rastreamento do carro 2 com MultiScaleTemplateMatch, offset default, com Filtro de Kalman:

    ```shell
    python tracker.py --car=2 --tracker=MSTM
    ```