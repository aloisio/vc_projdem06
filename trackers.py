import cv2
import numpy as np

class TemplateMatchTracker:
   def reset(self, img, bbox):
       self.bbox = bbox
       self.template = img[int(max(0,bbox[1])):int(max(0,bbox[3])), int(max(0,bbox[0])):int(max(0,bbox[2]))]

       if self.use_kalman:
           # init kalman filter object
           self.kalman = cv2.KalmanFilter(4, 2)
           self.kalman.measurementMatrix = np.array([[1, 0, 0, 0],
                                                [0, 1, 0, 0]], np.float32)

           self.kalman.transitionMatrix = np.array([[1, 0, 1, 0],
                                               [0, 1, 0, 1],
                                               [0, 0, 1, 0],
                                               [0, 0, 0, 1]], np.float32)

           self.kalman.processNoiseCov = np.array([[1, 0, 0, 0],
                                              [0, 1, 0, 0],
                                              [0, 0, 1, 0],
                                              [0, 0, 0, 1]], np.float32) * 0.03

           p1 = (int(bbox[0]), int(bbox[1]))
           p2 = (int(bbox[2]), int(bbox[3]))

           pcenter = np.array([(p1[0] + p2[0]) / 2., (p1[1] + p2[1]) / 2.], np.float32)

           #center KalmanFilter In Image
           for i in range(50):
               self.kalman.correct(pcenter)
               self.prediction = self.kalman.predict();


   def __init__(self, img, bbox, use_kalman=True, search_offset=3):
       self.offset = search_offset
       self.use_kalman = use_kalman
       self.reset(img, bbox)


   def update(self, img):


       x1 =  self.bbox[0]-self.offset
       if x1<0:
           x1=0
           x2=x1 + self.template.shape[1]+2*self.offset
       else:
           x2 = self.bbox[2] + self.offset
           if x2 > img.shape[1]:
               x2 = img.shape[1]
               x1 = x2 - self.template.shape[1]-2*self.offset

       y1 =  self.bbox[1]-self.offset
       if y1<0:
           y1=0
           y2=y1 + self.template.shape[0]+2*self.offset
       else:
           y2 = self.bbox[3] + self.offset
           if y2 > img.shape[0]:
               y2 = img.shape[0]
               y1 = y2 - self.template.shape[0]-2*self.offset
               
       search_box =   (int(x1), int(y1), int(x2), int(y2))      

       search_image = img[ int(y1):int(y2), int(x1):int(x2)]

       res = cv2.matchTemplate(search_image,self.template, cv2.TM_SQDIFF_NORMED)
       min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

       dx = x1 + min_loc[0]
       dy = y1 + min_loc[1]

       t_bbox = (dx, dy, dx+self.template.shape[1], dy+self.template.shape[0] )
       
       
       if self.use_kalman:

           # Kalman Filter

           w = self.template.shape[1]
           h = self.template.shape[0]

           pcenter = np.array([t_bbox[0]+ w/2., t_bbox[1]+h/ 2.], np.float32)

           self.kalman.correct(pcenter)
           prediction = self.kalman.predict()

           k1 = (prediction[0] - (0.5 * w), prediction[1] - (0.5 * h))
           k2 = (prediction[0] + (0.5 * w), prediction[1] + (0.5 * h))

           k_bbox = (k1[0], k1[1], k2[0], k2[1] )
       else:
           k_bbox = t_bbox

       self.bbox = k_bbox

       return self.bbox, t_bbox, search_box


class MultiScaleTMTracker:
    def reset(self, img, bbox):
        self.bbox = bbox
        template0 = img[int(max(0,bbox[1])):int(max(0,bbox[3])), int(max(0,bbox[0])):int(max(0,bbox[2]))]

        self.template = [
            cv2.resize(template0, None, fx=0.81450625, fy=0.81450625, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=0.857375, fy=0.857375, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=0.9025, fy=0.9025, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=0.95, fy=0.95, interpolation=cv2.INTER_CUBIC),
            template0,
            cv2.resize(template0, None, fx=1.05, fy=1.05, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=1.1025, fy=1.1025, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=1.157625, fy=1.157625, interpolation=cv2.INTER_CUBIC),
            cv2.resize(template0, None, fx=1.21550625, fy=1.21550625, interpolation=cv2.INTER_CUBIC)
        ]

        if self.use_kalman:
            # init kalman filter object
            self.kalman = cv2.KalmanFilter(4, 2)
            self.kalman.measurementMatrix = np.array([[1, 0, 0, 0],
                                                      [0, 1, 0, 0]], np.float32)

            self.kalman.transitionMatrix = np.array([[1, 0, 1, 0],
                                                     [0, 1, 0, 1],
                                                     [0, 0, 1, 0],
                                                     [0, 0, 0, 1]], np.float32)

            self.kalman.processNoiseCov = np.array([[1, 0, 0, 0],
                                                    [0, 1, 0, 0],
                                                    [0, 0, 1, 0],
                                                    [0, 0, 0, 1]], np.float32) * 0.03

            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[2]), int(bbox[3]))

            pcenter = np.array([(p1[0] + p2[0]) / 2., (p1[1] + p2[1]) / 2.], np.float32)

            # center KalmanFilter In Image
            for i in range(50):
                self.kalman.correct(pcenter)
                self.prediction = self.kalman.predict();

    def __init__(self, img, bbox, use_kalman=True, search_offset=3):
        self.offset = search_offset
        self.use_kalman = use_kalman
        self.reset(img, bbox)

    def update(self, img):

        x1 = self.bbox[0] - self.offset
        if x1 < 0:
            x1 = 0
            x2 = x1 + self.template[2].shape[1] + 2 * self.offset
        else:
            x2 = self.bbox[2] + self.offset
            if x2 > img.shape[1]:
                x2 = img.shape[1]
                x1 = x2 - self.template[2].shape[1] - 2 * self.offset

        y1 = self.bbox[1] - self.offset
        if y1 < 0:
            y1 = 0
            y2 = y1 + self.template[2].shape[0] + 2 * self.offset
        else:
            y2 = self.bbox[3] + self.offset
            if y2 > img.shape[0]:
                y2 = img.shape[0]
                y1 = y2 - self.template[2].shape[0] - 2 * self.offset

        search_box = (int(x1), int(y1), int(x2), int(y2))

        search_image = img[int(y1):int(y2), int(x1):int(x2)]

        m_min_val = 1
        for t in range(9):
            if self.template[t].shape[0] < search_image.shape[0] and self.template[t].shape[1] < search_image.shape[1]:
                res = cv2.matchTemplate(search_image, self.template[t], cv2.TM_SQDIFF_NORMED)
                min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
                if min_val <= m_min_val:
                    m_min_val = min_val
                    m_min_loc = min_loc
                    m_t = t

        dx = x1 + m_min_loc[0]
        dy = y1 + m_min_loc[1]

        t_bbox = (dx, dy, dx + self.template[m_t].shape[1], dy + self.template[m_t].shape[0])

        if self.use_kalman:

            # Kalman Filter

            w = self.template[m_t].shape[1]
            h = self.template[m_t].shape[0]

            pcenter = np.array([t_bbox[0] + w / 2., t_bbox[1] + h / 2.], np.float32)

            self.kalman.correct(pcenter)
            prediction = self.kalman.predict()

            k1 = (prediction[0] - (0.5 * w), prediction[1] - (0.5 * h))
            k2 = (prediction[0] + (0.5 * w), prediction[1] + (0.5 * h))

            k_bbox = (k1[0], k1[1], k2[0], k2[1])
        else:
            k_bbox = t_bbox

        self.bbox = k_bbox

        return self.bbox, t_bbox, search_box


class MeanShiftTracker:

   def reset(self, img, bbox):
       self.bbox = bbox
       self.template = img[int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])]
       p1 = (int(bbox[0]), int(bbox[1]))
       p2 = (int(bbox[2]), int(bbox[3]))
       self.track_window = (p1[0], p1[1], p2[0] - p1[0], p2[1] - p1[1])

       vis = img.copy()
       hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
       mask = cv2.inRange(hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)))

       hsv_roi = hsv[p1[1]:p2[1], p1[0]:p2[0]]
       mask_roi = mask[p1[1]:p2[1], p1[0]:p2[0]]
       hist = cv2.calcHist([hsv_roi], [0], mask_roi, [16], [0, 180])
       cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX)

       self.hist = hist.reshape(-1)
       self.show_hist()


       self.bbox = bbox
       self.template = img[int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])]


       vis_roi = vis[p1[1]:p2[1], p1[0]:p2[0]]
       cv2.bitwise_not(vis_roi, vis_roi)
       vis[mask == 0] = 0


       if self.use_kalman:
           # init kalman filter object
           self.kalman = cv2.KalmanFilter(4, 2)
           self.kalman.measurementMatrix = np.array([[1, 0, 0, 0],
                                                [0, 1, 0, 0]], np.float32)

           self.kalman.transitionMatrix = np.array([[1, 0, 1, 0],
                                               [0, 1, 0, 1],
                                               [0, 0, 1, 0],
                                               [0, 0, 0, 1]], np.float32)

           self.kalman.processNoiseCov = np.array([[1, 0, 0, 0],
                                              [0, 1, 0, 0],
                                              [0, 0, 1, 0],
                                              [0, 0, 0, 1]], np.float32) * 0.03


           pcenter = np.array([(p1[0] + p2[0]) / 2., (p1[1] + p2[1]) / 2.], np.float32)

           #center KalmanFilter In Image
           for i in range(50):
               self.kalman.correct(pcenter)
               self.prediction = self.kalman.predict();

   def __init__(self, img, bbox, use_kalman=True):

       self.term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
       self.use_kalman = use_kalman
       self.reset(img, bbox)


   def show_hist(self):
       bin_count = self.hist.shape[0]
       bin_w = 24
       img = np.zeros((256, bin_count * bin_w, 3), np.uint8)
       for i in range(bin_count):
           h = int(self.hist[i])
           cv2.rectangle(img, (i * bin_w + 2, 255), ((i + 1) * bin_w - 2, 255 - h),
                        (int(180.0 * i / bin_count), 255, 255), -1)
       img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
       cv2.imshow('hist', img)

   def update(self, img):

       w = self.template.shape[1]
       h = self.template.shape[0]


       p1 = (int(self.bbox[0]), int(self.bbox[1]))
       p2 = (int(self.bbox[2]), int(self.bbox[3]))

       vis = img.copy()
       hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
       mask = cv2.inRange(hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)))

       prob = cv2.calcBackProject([hsv], [0], self.hist, [0, 180], 1)
       prob &= mask

       track_box, self.track_window = cv2.CamShift(prob, self.track_window, self.term_crit)

       xt,yt,wt,ht = self.track_window

       x1=xt+wt/2-w/2
       x2=xt+wt/2+w/2
       y1=yt+ht/2-h/2
       y2=yt+ht/2+h/2

       search_box = (xt, yt, xt+wt,yt+ht)

       t_bbox = (x1, y1, x2, y2)

       if self.use_kalman:

           # Kalman Filter

           pcenter = np.array([t_bbox[0] + w / 2., t_bbox[1] + h / 2.], np.float32)

           self.kalman.correct(pcenter)
           prediction = self.kalman.predict()

           k1 = (prediction[0] - (0.5 * w), prediction[1] - (0.5 * h))
           k2 = (prediction[0] + (0.5 * w), prediction[1] + (0.5 * h))

           k_bbox = (k1[0], k1[1], k2[0], k2[1])
           xt, yt, wt, ht = self.track_window

           self.track_window = (prediction[0] - wt / 2, prediction[1] - ht / 2, wt, ht)

       else:
           k_bbox = t_bbox

       self.bbox = k_bbox



       return self.bbox, k_bbox, search_box
